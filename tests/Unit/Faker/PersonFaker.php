<?php


namespace Tests\Unit\ImkDataFields\Faker;

use ImkDataFields\Traits\Person\CivilityTrait;
use ImkDataFields\Traits\Person\IdentityTrait;
use ImkDataFields\Traits\Person\PseudoTrait;
use ImkDataFields\Traits\Person\SexTrait;

/**
 * Class PersonFaker
 *
 * @package Tests\Unit\ImkDataFields\Faker
 */
class PersonFaker
{
    use CivilityTrait;

    use SexTrait;

    use IdentityTrait;

    use PseudoTrait;
}
