<?php

namespace Tests\Unit\ImkDataFields\Interfaces;

/**
 * Class TestInt.
 */
interface TestInterface
{
    public function test(array $expect);

    public function provide():\Generator;
}
