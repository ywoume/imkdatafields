<?php


namespace Tests\Unit\ImkDataFields\Model;

use ImkDataFields\Model\Person\CivilityTrait;
use ImkDataFields\Model\Person\IdentityTrait;
use ImkDataFields\Model\Person\PseudoTrait;
use ImkDataFields\Model\Person\SexTrait;

/**
 * Class PersonTraitFaker.
 */
class PersonTraitFaker
{
    use CivilityTrait;

    use SexTrait;

    use IdentityTrait;

    use PseudoTrait;
}
