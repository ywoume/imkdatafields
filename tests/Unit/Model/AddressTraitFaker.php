<?php


namespace Tests\Unit\ImkDataFields\Model;

use ImkDataFields\Model\Address\CityTrait;
use ImkDataFields\Model\Address\CountryTrait;
use ImkDataFields\Model\Address\PostalCodeTrait;
use ImkDataFields\Model\Address\StreetTrait;

/**
 * Class AddressTraitFaker.
 */
final class AddressTraitFaker
{
    use StreetTrait;

    use PostalCodeTrait;

    use CountryTrait;

    use CityTrait;
}
