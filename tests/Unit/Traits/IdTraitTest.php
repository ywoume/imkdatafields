<?php


namespace Tests\Unit\ImkDataFields\Traits;

use ImkDataFields\Traits\Id\IdTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class IdTraitFakerTest.
 * @covers IdTrait
 */
class IdTraitTest extends TestCase
{
    /**
     * @dataProvider provide
     * @param array $expect
     */
    public function test(array $expect)
    {
        $traitFaker = $this->getMockForTrait(IdTrait::class);

        $traitFaker->setId($expect['id']);
        $this->assertIsInt($expect['id']);
        $this->assertEquals($expect['id'], $traitFaker->getId());
    }

    public function provide(): \Generator
    {
        yield  [
            ['id' => 1],
        ];
        yield  [
            ['id' => 13333],
        ];
    }
}
