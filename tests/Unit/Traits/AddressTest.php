<?php


namespace Tests\Unit\ImkDataFields\Traits;

use PHPUnit\Framework\TestCase;
use Tests\Unit\ImkDataFields\Faker\AddressFaker;
use Tests\Unit\ImkDataFields\Interfaces\TestInterface;

/**
 * Class AddressTraitFakerTest.
 */
class AddressTest extends TestCase implements TestInterface
{
    /**
     * @param array $expect
     * @dataProvider provide
     */
    public function test(array $expect)
    {
        $address = new AddressFaker();

        $address->setCityName($expect['cityName']);
        $address->setStreetName($expect['streetName']);
        $address->setStreetNumber($expect['streetNumber']);
        $address->setPostalCode($expect['postalCode']);
        $address->setCountry($expect['country']);

        $this->assertEquals($expect['cityName'], $address->getCityName());
        $this->assertEquals($expect['streetName'], $address->getStreetName());
        $this->assertEquals($expect['streetNumber'], $address->getStreetNumber());
        $this->assertEquals($expect['postalCode'], $address->getPostalCode());
        $this->assertEquals($expect['country'], $address->getCountry());
    }

    public function provide(): \Generator
    {
        yield  [
            [
                'cityName' => 'cityName',
                'streetName' => 'streetName',
                'streetNumber' => 32,
                'postalCode' => '75699',
                'country' => 'country',
            ]
        ];
    }
}
