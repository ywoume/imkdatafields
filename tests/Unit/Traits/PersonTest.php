<?php

namespace Tests\Unit\ImkDataFields\Traits;

use PHPUnit\Framework\TestCase;
use Tests\Unit\ImkDataFields\Faker\PersonFaker;
use Tests\Unit\ImkDataFields\Interfaces\TestInterface;

/**
 * Class PersonTest
 *
 * @package Tests\Unit\ImkDataFields\Traits
 */
class PersonTest extends TestCase implements TestInterface
{
    /**
     * @param array $expect
     *
     * @dataProvider provide
     *
     * @covers  ::setFirstName
     * @covers  ::getFirstName
     * @covers  ::setLastName
     * @covers  ::getLastName
     * @covers  ::setPseudo
     * @covers  ::getPseudo
     * @covers  ::setCivility
     * @covers  ::getCivility
     * @covers  ::setSex
     * @covers  ::getSex
     */
    public function test(array $expect)
    {
        $person = new PersonFaker();
        $person->setFirstName($expect['firstName']);
        $person->setLastName($expect['lastName']);
        $person->setSex($expect['sex']);
        $person->setCivility($expect['civility']);
        $person->setPseudo($expect['pseudo']);

        $this->assertIsString($expect['firstName']);
        $this->assertIsString($expect['lastName']);
        $this->assertIsString($expect['civility']);
        $this->assertIsString($expect['pseudo']);

        $this->assertIsBool($expect['sex']);

        $this->assertEquals($expect['firstName'], $person->getFirstName());
        $this->assertEquals($expect['lastName'], $person->getLastName());
        $this->assertEquals($expect['pseudo'], $person->getPseudo());
        $this->assertEquals($expect['civility'], $person->getCivility());
        $this->assertEquals($expect['sex'], $person->getSex());
    }

    public function provide(): \Generator
    {
        yield [
           [
               'firstName' => 'firstName',
               'lastName' => 'lastName',
               'sex' => true,
               'civility' => 'civility',
               'pseudo' => 'pseudo',
           ]
       ];
    }
}
