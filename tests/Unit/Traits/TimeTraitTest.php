<?php


namespace Tests\Unit\ImkDataFields\Traits;

use ImkDataFields\Traits\Time\TimestampableTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class TimeTraitFaker.
 */
class TimeTraitTest extends TestCase
{
    public function test()
    {
        $mockForTrait = $this->getMockForTrait(TimestampableTrait::class);

        $mockForTrait->setCreatedAt(new \DateTime('2020-10-12 00:01:01'));
        $mockForTrait->setUpdatedAt(new \DateTime('2020-10-12 00:01:01'));

        $this->assertEquals('2020-10-12 00:01:01', $mockForTrait->getCreatedAt()->format('Y-m-d H:i:s'));
        $this->assertEquals('2020-10-12 00:01:01', $mockForTrait->getUpdatedAt()->format('Y-m-d H:i:s'));
    }
}
