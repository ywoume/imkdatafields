<?php

namespace ImkDataFields\Model\Address;

/**
 * Trait PostalCodeTrait
 *
 * @package ImkDataFields\Model\Address
 */
trait PostalCodeTrait
{

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @return string|null
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     */
    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }
}
