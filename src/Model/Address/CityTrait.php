<?php


namespace ImkDataFields\Model\Address;

/**
 * Trait CityTrait
 *
 * @package ImkDataFields\Model\Address
 */
trait CityTrait
{
    /**
     * @var string|null
     */
    private $cityName;

    /**
     * @return string|null
     */
    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    /**
     * @param string|null $cityName
     */
    public function setCityName(?string $cityName): void
    {
        $this->cityName = $cityName;
    }
}
