<?php

namespace ImkDataFields\Model\Address;

/**
 * Trait CountryTrait
 *
 * @package ImkDataFields\Model\Address
 */
trait CountryTrait
{

    /**
     * @var string|null
     */
    private $country;

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }
}
