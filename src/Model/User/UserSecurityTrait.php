<?php


namespace ImkDataFields\Model\User;

use ImkDataFields\Model\Common\EmailTrait;
use ImkDataFields\Model\Common\UsernameTrait;
use ImkDataFields\Model\Enabled\EnabledTrait;

trait UserSecurityTrait
{
    use UsernameTrait;

    use PasswordTrait;

    use EmailTrait;

    use EnabledTrait;



    /**
     * @var string|null
     */
    private $salt;

    /**
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @param string|null $salt
     */
    public function setSalt(?string $salt): void
    {
        $this->salt = $salt;
    }
}
