<?php

namespace ImkDataFields\Model\User;

/**
 * Trait UserTrait
 *
 * @package ImkDataFields\Model\User
 */
trait UserTrait
{
    private $user;

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }
}
