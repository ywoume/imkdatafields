<?php

namespace ImkDataFields\Model\Person;

/**
 * Trait CivilityTrait
 *
 * @package ImkDataFields\Model\Person
 */
trait CivilityTrait
{
    /**
     * @var string|null
     */
    private $civility;

    /**
     * @return string|null
     */
    public function getCivility(): ?string
    {
        return $this->civility;
    }

    /**
     * @param string $civility
     */
    public function setCivility(string $civility): void
    {
        $this->civility = $civility;
    }
}
