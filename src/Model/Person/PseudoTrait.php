<?php

namespace ImkDataFields\Model\Person;

/**
 * Trait PseudoTrait
 *
 * @package ImkDataFields\Model\Person
 */
trait PseudoTrait
{
    /**
     * @var string|null
     */
    private $pseudo;

    /**
     * @return string|null
     */
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    /**
     * @param string|null $pseudo
     */
    public function setPseudo(?string $pseudo): void
    {
        $this->pseudo = $pseudo;
    }
}
