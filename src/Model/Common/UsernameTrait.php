<?php


namespace ImkDataFields\Model\Common;

trait UsernameTrait
{

    /**
     * @var string|null
     */
    private $username;

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }
}
