<?php

namespace ImkDataFields\Model\Token;

/**
 * Trait TokenTmpTrait
 *
 * @package ImkDataFields\Model\Token
 */
trait TokenTmpTrait
{
    /**
     * @var string
     */
    private $tokenTmp;

    /**
     * @var \DateTimeInterface|null
     */
    private $tokenExpiredAt;

    public function getTokenTmp(): ?string
    {
        return $this->tokenTmp;
    }

    public function setTokenTmp(): void
    {
        $this->tokenTmp = uniqid().sha1('xmen').uniqid();
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTokenExpiredAt(): ?\DateTimeInterface
    {
        return $this->tokenExpiredAt;
    }

    public function setTokenExpiredAt(): self
    {
        $this->tokenExpiredAt = new \DateTime('now');
    }
}
