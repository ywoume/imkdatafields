<?php


namespace ImkDataFields\Model\Token;

/**
 * Trait TokenTrait
 *
 * @package ImkDataFields\Model\Token
 */
trait TokenTrait
{
    /**
     * @var string|null
     */
    private $token;

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     *
     * @return $this
     */
    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
