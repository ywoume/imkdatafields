<?php

namespace ImkDataFields\Model\Enabled;

/**
 * Trait EnabledTrait
 *
 * @package ImkDataFields\Model\Enabled
 */
trait EnabledTrait
{
    /**
     * @var bool
     */
    private $isEnable = false;

    /**
     * @return bool
     */
    public function getIsEnable(): bool
    {
        return $this->isEnable;
    }

    /**
     * @param bool $isEnable
     */
    public function setIsEnable(bool $isEnable): void
    {
        $this->isEnable = $isEnable;
    }
}
